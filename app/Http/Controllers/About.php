<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class About extends Controller
{
    public function show()
    {
        return response('About: Blank')->header('Content-Type', 'text/plain');
    }
}
