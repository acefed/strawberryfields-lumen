<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Inbox extends Controller
{
    public function show()
    {
        return abort(405);
    }

    public function store($username, Request $request)
    {
        $hostname = parse_url($this::$CONFIG['origin'], PHP_URL_HOST);
        $content_type_header_field = $request->header('Content-Type');
        $has_type = false;
        $y = $request->input();
        $t = $y['type'] ?? '';
        $aid = $y['id'] ?? '';
        $atype = $y['type'] ?? '';
        if (strlen($aid) > 1024 || strlen($atype) > 64) return abort(400);
        error_log("INBOX {$aid} {$atype}");
        if ($username !== $this::$CONFIG['actor'][0]['preferredUsername']) return abort(404);
        if (strpos($content_type_header_field, 'application/activity+json') !== false) $has_type = true;
        if (strpos($content_type_header_field, 'application/ld+json') !== false) $has_type = true;
        if (strpos($content_type_header_field, 'application/json') !== false) $has_type = true;
        if (!$has_type) return abort(400);
        if (!$request->header('Digest') || !$request->header('Signature')) return abort(400);
        if ($t === 'Accept' || $t === 'Reject' || $t === 'Add') return response('', 200);
        if ($t === 'Remove' || $t === 'Like' || $t === 'Announce') return response('', 200);
        if ($t === 'Create' || $t === 'Update' || $t === 'Delete') return response('', 200);
        if ($t === 'Follow') {
            if (parse_url($y['actor'] ?? '', PHP_URL_SCHEME) !== 'https') return abort(400);
            $x = $this::getActivity($username, $hostname, $y['actor']);
            if (!$x) return abort(500);
            $this::acceptFollow($username, $hostname, $x, $y);
            return response('', 200);
        }
        if ($t === 'Undo') {
            $z = $y['object'] ?? [];
            $t = $z['type'] ?? '';
            if ($t === 'Accept' || $t === 'Like' || $t === 'Announce') return response('', 200);
            if ($t === 'Follow') {
                if (parse_url($y['actor'] ?? '', PHP_URL_SCHEME) !== 'https') return abort(400);
                $x = $this::getActivity($username, $hostname, $y['actor']);
                if (!$x) return abort(500);
                $this::acceptFollow($username, $hostname, $x, $z);
                return response('', 200);
            }
        }
        return abort(500);
    }
}
