<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Home extends Controller
{
    public function index()
    {
        return response('StrawberryFields Lumen')->header('Content-Type', 'text/plain');
    }
}
