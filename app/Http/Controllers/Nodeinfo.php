<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Nodeinfo extends Controller
{
    public function show()
    {
        $hostname = parse_url($this::$CONFIG['origin'], PHP_URL_HOST);
        $body = [
            'links' => [
                [
                    'rel' => 'http://nodeinfo.diaspora.software/ns/schema/2.0',
                    'href' => "https://{$hostname}/nodeinfo/2.0.json",
                ],
                [
                    'rel' => 'http://nodeinfo.diaspora.software/ns/schema/2.1',
                    'href' => "https://{$hostname}/nodeinfo/2.1.json",
                ],
            ],
        ];
        $headers = [
            'Cache-Control' => "public, max-age={$this::$CONFIG['ttl']}, must-revalidate",
            'Vary' => 'Accept, Accept-Encoding',
            'Content-Type' => 'application/json',
        ];
        return response()->json($body, 200, $headers, JSON_UNESCAPED_SLASHES);
    }
}
