# StrawberryFields Lumen

- [3.0.0]
- [2.9.0] - 2024-08-10 - 11 files changed, 160 insertions(+), 94 deletions(-)
- [2.8.0] - 2024-03-10 - 5 files changed, 88 insertions(+), 23 deletions(-)
- [2.7.0] - 2024-02-04 - 9 files changed, 69 insertions(+), 23 deletions(-)
- [2.6.0] - 2023-11-11 - 6 files changed, 36 insertions(+), 20 deletions(-)
- [2.5.0] - 2023-11-05 - 20 files changed, 580 insertions(+), 404 deletions(-)
- [2.4.0] - 2023-04-16 - 9 files changed, 367 insertions(+), 391 deletions(-)
- [2.3.0] - 2022-11-27 - 10 files changed, 7479 insertions(+), 11 deletions(-)
- [2.2.0] - 2022-11-20 - 6 files changed, 9 insertions(+), 7 deletions(-)
- [2.1.0] - 2022-06-26 - 5 files changed, 12 insertions(+), 23 deletions(-)
- [2.0.0] - 2022-03-13 - 11 files changed, 295 insertions(+), 79 deletions(-)
- [1.5.0] - 2021-10-11 - 6 files changed, 49 insertions(+), 4 deletions(-)
- [1.4.0] - 2021-10-01 - 4 files changed, 9 insertions(+), 2 deletions(-)
- [1.3.0] - 2021-09-25 - 4 files changed, 10 insertions(+), 7 deletions(-)
- [1.2.0] - 2021-04-26 - 4 files changed, 14 insertions(+), 3 deletions(-)
- [1.1.0] - 2021-03-23 - 3 files changed, 4 insertions(+), 5 deletions(-)
- 1.0.0 - 2021-03-04

[3.0.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/7523f5cc...master
[2.9.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/a626c48a...7523f5cc
[2.8.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/2e8cdeda...a626c48a
[2.7.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/14409a52...2e8cdeda
[2.6.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/98b29a2d...14409a52
[2.5.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/8f16e704...98b29a2d
[2.4.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/c3506794...8f16e704
[2.3.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/132859bc...c3506794
[2.2.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/cd1460ea...132859bc
[2.1.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/b5544f95...cd1460ea
[2.0.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/65b899d7...b5544f95
[1.5.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/4e6e18d3...65b899d7
[1.4.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/29ae2504...4e6e18d3
[1.3.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/46accaaa...29ae2504
[1.2.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/6449066b...46accaaa
[1.1.0]: https://gitlab.com/acefed/strawberryfields-lumen/-/compare/9d5b35f1...6449066b
