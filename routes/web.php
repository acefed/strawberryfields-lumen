<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'Home@index');

$router->get('/about', 'About@show');

$router->get('u/{username}', 'UUser@show');

$router->get('u/{username}/inbox', 'Inbox@show');
$router->post('u/{username}/inbox', 'Inbox@store');

$router->post('u/{username}/outbox', 'Outbox@store');
$router->get('u/{username}/outbox', 'Outbox@show');

$router->get('u/{username}/following', 'Following@show');

$router->get('u/{username}/followers', 'Followers@show');

$router->post('s/{secret}/u/{username}', 'SSend@store');

$router->get('.well-known/nodeinfo', 'Nodeinfo@show');

$router->get('.well-known/webfinger', 'Webfinger@show');

$router->get('@', function () {
    return redirect('/');
});
$router->get('u', function () {
    return redirect('/');
});
$router->get('user', function () {
    return redirect('/');
});
$router->get('users', function () {
    return redirect('/');
});

$router->get('users/{username}', function ($username) {
    return redirect("u/{$username}");
});
$router->get('user/{username}', function ($username) {
    return redirect("u/{$username}");
});
$router->get('@{username}', function ($username) {
    return redirect("u/{$username}");
});
